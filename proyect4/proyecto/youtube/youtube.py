from xml.sax.handler import ContentHandler
from xml.sax import make_parser


class YTHandler(ContentHandler):

    def __init__(self):
        super().__init__()
        self.videos = []
        self.current_video = None
        self.in_entry = False
        self.in_content = False
        self.content = ""

    def startElement(self, name, attrs):
        if name == 'entry':
            self.in_entry = True
            self.current_video = {
                'link': '',
                'title': '',
                'id': '',
                'image': '',
                'published': '',
                'description': '',
                'name': '',
                'url': ''
            }
        elif self.in_entry:
            if name == 'title' or name == 'yt:videoId' or name == 'published' or name == 'media:description' or name == 'name' or name == 'uri':
                self.in_content = True
            elif name == 'link':
                self.current_video['link'] = attrs.get('href')
            elif name == 'media:thumbnail':
                self.current_video['image'] = attrs.get('url')

    def endElement(self, name):
        if name == 'entry':
            self.in_entry = False
            self.videos.append(self.current_video)
            self.current_video = None
        elif self.in_entry:
            if name == 'title':
                self.current_video['title'] = self.content.strip()
            elif name == 'yt:videoId':
                self.current_video['id'] = self.content.strip()
            elif name == 'published':
                self.current_video['published'] = self.content.strip()
            elif name == 'media:description':
                self.current_video['description'] = self.content.strip()
            elif name == 'name':
                self.current_video['name'] = self.content.strip()
            elif name == 'uri':
                self.current_video['url'] = self.content.strip()
            self.content = ""
            self.in_content = False

    def characters(self, content):
        if self.in_content:
            self.content += content


class YTChannel:

    def __init__(self, stream):
        self.parser = make_parser()
        self.handler = YTHandler()
        self.parser.setContentHandler(self.handler)
        try:
            self.parser.parse(stream)
        except Exception as e:
            print(f"Error parsing XML: {e}")

    def videos(self):
        return self.handler.videos
