from django.shortcuts import render
from django.http import HttpResponse
from django.middleware.csrf import get_token
from . import form

def construir_html(videos, name, action, token):
    html = ""
    for video in videos:
        html += form.VIDEO.format(
            link=video['link'],
            title=video['title'],
            id=video['id'],
            name=name,
            action=action,
            token=token,
            image=video['image'],
            published=video['published'],
            description=video['description'],
            url=video['url']
        )
    return html

def mover_video(from_list, to_list, id):
    for i, video in enumerate(from_list):
        if video['id'] == id:
            to_list.append(from_list.pop(i))
            return
        
def videos(request, id):
    encontrado = None
    if request.method == 'GET':
        for video in form.selected:
            if video['id'] == id:
                encontrado = video
                break

    if encontrado:
        htmlBody = form.INFORMACION.format(video=encontrado)
    else:
        htmlBody = form.ERROR.format(id=id)

    return HttpResponse(htmlBody)


def index(request):
    if request.method == 'POST':
        id = request.POST.get('id')
        if id:
            if request.POST.get('select'):
                mover_video(from_list=form.selectable, to_list=form.selected, id=id)
            elif request.POST.get('deselect'):
                mover_video(from_list=form.selected, to_list=form.selectable, id=id)

    csrf_token = get_token(request)
    selected_html = construir_html(form.selected, name='deselect', action='Deseleccionar', token=csrf_token)
    selectable_html = construir_html(form.selectable, name='select', action='Seleccionar', token=csrf_token)
    htmlBody = form.PAGE.format(selected=selected_html, selectable=selectable_html)
    return HttpResponse(htmlBody)
